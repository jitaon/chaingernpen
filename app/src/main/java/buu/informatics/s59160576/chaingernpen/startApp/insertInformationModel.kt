package buu.informatics.s59160576.chaingernpen.startApp

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import buu.informatics.s59160576.chaingernpen.database.*
import kotlinx.coroutines.launch

class insertInformationModel(application: Application) : AndroidViewModel(application) {


    private val repository:InsertInformationRepository
    val allUser: LiveData<List<userinformation>>

    init {
        val wordsDao = userinformationDatabase.getDatabase(application.applicationContext).userinformationDatabaseDao()
        repository = InsertInformationRepository(wordsDao)
        allUser = repository.getAllUser

    }


    fun insert(user: userinformation) = viewModelScope.launch {
        repository.insert(user)
    }

}

class insertSavingCouseModel(application: Application) : AndroidViewModel(application) {


    private val repository:InsertSavingCouseRepository
    val allSavingCouse: LiveData<List<user_saving>>

    init {
        val wordsDao = userSavingDatabase.getDatabase(application.applicationContext).savingDatabaseDao()
        repository = InsertSavingCouseRepository(wordsDao)
        allSavingCouse = repository.getAllSaving

    }


    fun insert(saving: user_saving) = viewModelScope.launch {
        repository.insert(saving)
    }
    fun update(saving: user_saving) = viewModelScope.launch {
        repository.update(saving)
    }

}
