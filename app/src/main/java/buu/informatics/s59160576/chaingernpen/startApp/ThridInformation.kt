package buu.informatics.s59160576.chaingernpen.startApp


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.informatics.s59160576.chaingernpen.R
import buu.informatics.s59160576.chaingernpen.databinding.ThirdInformationPageBinding
import buu.informatics.s59160576.chaingernpen.information_user2

/**
 * A simple [Fragment] subclass.
 */
class ThridInformation : Fragment() {

    private lateinit var binding: ThirdInformationPageBinding
    private var insert_information_user: ArrayList<information_user2> = ArrayList<information_user2>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.third_information_page, container, false)

        binding.button5.setOnClickListener { view : View ->
            checkData()
        }

        binding.button2.setOnClickListener { view: View ->
//            view?.findNavController()?.navigate(ThridInformationDirections.actionThridInformationToSecondInformation(
//
//            ))
        }

        return binding.root
    }

    private fun checkData() {
        var model = arguments?.getString("secondData")
        if(binding.homeCouse.text.isEmpty()){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else if(binding.carCouse.text.isEmpty()){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else if(binding.normalCouse.text.isEmpty()){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else if(binding.creditCouse.text.isEmpty()){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else{

            view?.findNavController()?.navigate(ThridInformationDirections.actionThridInformationToLastInformation(
                model.toString()+" "+binding.homeCouse.text.toString() + " "+binding.carCouse.text.toString()+ " "
                        +binding.normalCouse.text.toString()+ " " +binding.creditCouse.text.toString()
            ))
        }
    }


}
