package buu.informatics.s59160576.chaingernpen.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_information")
data class userinformation(
    @PrimaryKey(autoGenerate = true)
    var userId: Long = 0L,

    @ColumnInfo(name = "sex_user_data")
    var sex:String,

    @ColumnInfo(name = "age_user_data")
    var age:Int = 0,

    @ColumnInfo(name = "target_user_data")
    var target:Int = 0,

    @ColumnInfo(name = "job_user_data")
    var job:String = "",

    @ColumnInfo(name = "job_age_user_data")
    var job_age:String = "",

    @ColumnInfo(name = "start_salary_user_data")
    var start_salary:Int = 0,

    @ColumnInfo(name = "salary_user_data")
    var salary:Int = 0,

    @ColumnInfo(name = "home_couse_user_data")
    var home_couse:Int = 0,

    @ColumnInfo(name = "car_couse_user_data")
    var car_couse:Int = 0,

    @ColumnInfo(name = "normal_couse_user_data")
    var normall_couse:Int=0,

    @ColumnInfo(name = "credit_couse_user_data")
    var credit_couse:Int = 0
)

@Entity(tableName = "user_saving")
data class user_saving(
    @PrimaryKey(autoGenerate = true)
    var savingID : Long = 0L,

    @ColumnInfo(name = "userID")
    var userID:Long = 0L,

    @ColumnInfo(name = "saving_couse")
    var savingCouse:Int = 0,

    @ColumnInfo(name = "invest_couse")
    var investCouse:Int = 0,

    @ColumnInfo(name = "use_couse")
    var useCouse:Int = 0,

    @ColumnInfo(name = "count_donw_day")
    var countDonwDay:Int = 0,

    @ColumnInfo(name = "saving_money")
    var savingMoney:Int = 0


)