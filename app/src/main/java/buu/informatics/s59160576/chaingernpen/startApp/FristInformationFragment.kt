package buu.informatics.s59160576.chaingernpen.startApp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import buu.informatics.s59160576.chaingernpen.R
import buu.informatics.s59160576.chaingernpen.databinding.FristInformationPageBinding
import buu.informatics.s59160576.chaingernpen.information_user


class FristInformationFragment : Fragment(){
    private lateinit var binding: FristInformationPageBinding

    private lateinit var viewModel: insertInformationModel
    //private var insert_information_user: ArrayList<information_user> = ArrayList<information_user>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.frist_information_page, container, false)

        viewModel = ViewModelProviders.of(this).get(insertInformationModel::class.java)
        binding.button3.setOnClickListener {
            checkData()
        }

        return binding.root

    }

    private fun insertData(){

        var sex =  binding.sexInput.text.toString()
        var age = binding.ageInput.text.toString()
        var target = binding.targetInput.text.toString()
        //insert_information_user.add(0, information_user(sex,age,target))
        view?.findNavController()?.navigate(FristInformationFragmentDirections.actionFristInformationToSecondInformation(
            sex+" "+age+" "+target
        ))


    }
    fun checkData(){
        if(binding.sexInput.text.toString() == ""){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else if(binding.ageInput.text.toString() == ""){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else if(binding.targetInput.text.toString() == ""){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else{
            insertData()
        }
    }
}