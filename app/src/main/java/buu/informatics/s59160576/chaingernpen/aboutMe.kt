package buu.informatics.s59160576.chaingernpen


import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160576.chaingernpen.databinding.AboutMeBinding
import buu.informatics.s59160576.chaingernpen.databinding.MainPageBinding

/**
 * A simple [Fragment] subclass.
 */
class aboutMe : Fragment() {
    private lateinit var binding: AboutMeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.about_me, container, false)
        binding.but.setOnClickListener {
            onShare()
        }
        return binding.root
    }

    private fun onShare() {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, "ใช้เงินเป็น")
        startActivity(Intent(shareIntent))
        try {
            startActivity(shareIntent)
        } catch (ex: ActivityNotFoundException) {

        }
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }


}
