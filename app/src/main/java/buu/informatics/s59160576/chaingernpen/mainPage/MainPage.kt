package buu.informatics.s59160576.chaingernpen.mainPage


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160576.chaingernpen.R
import buu.informatics.s59160576.chaingernpen.databinding.MainPageBinding
import buu.informatics.s59160576.chaingernpen.startApp.SecondInformationDirections
import buu.informatics.s59160576.chaingernpen.startApp.insertInformationModel
import buu.informatics.s59160576.chaingernpen.startApp.insertSavingCouseModel

/**
 * A simple [Fragment] subclass.
 */
class MainPage : Fragment() {
    private lateinit var viewModel: insertInformationModel
    private lateinit var viewModel2: insertSavingCouseModel
    private lateinit var binding: MainPageBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProviders.of(this).get(insertInformationModel::class.java)
        viewModel2 = ViewModelProviders.of(this).get(insertSavingCouseModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.main_page, container, false)
        checkInformation()


        viewModel2.allSavingCouse.observe(this, Observer { saving ->
            saving.map {
                saving.forEach {
                    Log.i("check",it.toString())
                }
                    binding.nowSavingMoney.text = it.savingMoney.toString()
                    if(it.countDonwDay == 0 || it.savingMoney >= 50000){
                        binding.dateCountDonwNow2.text = "ยินดีด้วย"
                        binding.dateCountDonwNow.text = "คุณทำสำเร็จแล้ว"
                    }else{
                        binding.dateCountDonwNow.text = it.countDonwDay.toString() + " เดือน"
                    }

            }
        })

        binding.toSavingPage.setOnClickListener {
            view?.findNavController()?.navigate(MainPageDirections.actionMainPageToSavingPage())
        }



        setHasOptionsMenu(true)
        return binding.root
    }

    private fun checkInformation() {
        viewModel.allUser.observe(this, Observer { user ->
                if(user.isEmpty()){
                    view?.findNavController()?.navigate((MainPageDirections.actionMainPageToFristInformation()))
                }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }
}
