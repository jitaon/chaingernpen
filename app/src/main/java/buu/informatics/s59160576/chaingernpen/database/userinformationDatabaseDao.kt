package buu.informatics.s59160576.chaingernpen.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface userinformationDatabaseDao{
    @Insert
    fun insert(user: userinformation)

    @Update
    fun update(user: userinformation)

    @Query("SELECT * from user_information WHERE userId = :key")
    fun get(key: Long): userinformation?

    @Query("DELETE FROM user_information")
    fun clear()


    @Query("SELECT * FROM user_information ORDER BY userId DESC")
    fun getAllUser(): LiveData<List<userinformation>>
}