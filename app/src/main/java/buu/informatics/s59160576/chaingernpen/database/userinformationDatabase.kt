package buu.informatics.s59160576.chaingernpen.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(userinformation::class), version = 1, exportSchema = false)
abstract class userinformationDatabase : RoomDatabase() {

    abstract fun userinformationDatabaseDao(): userinformationDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: userinformationDatabase? = null

        fun getDatabase(
            context: Context
        ): userinformationDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    userinformationDatabase::class.java,
                    "user_database"
                ).allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

}

@Database(entities = arrayOf(user_saving::class), version = 1, exportSchema = false)
abstract class userSavingDatabase : RoomDatabase() {

    abstract fun savingDatabaseDao(): savingDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: userSavingDatabase? = null

        fun getDatabase(
            context: Context
        ): userSavingDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    userSavingDatabase::class.java,
                    "saving_database"
                ).allowMainThreadQueries()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

}

