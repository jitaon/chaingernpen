package buu.informatics.s59160576.chaingernpen.saving


import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160576.chaingernpen.R
import buu.informatics.s59160576.chaingernpen.database.user_saving
import buu.informatics.s59160576.chaingernpen.databinding.MainPageBinding
import buu.informatics.s59160576.chaingernpen.databinding.SavingPageBinding
import buu.informatics.s59160576.chaingernpen.startApp.insertInformationModel
import buu.informatics.s59160576.chaingernpen.startApp.insertSavingCouseModel

/**
 * A simple [Fragment] subclass.
 */
class SavingPage : Fragment() {

    private lateinit var viewModel: insertInformationModel
    private lateinit var viewModel2: insertSavingCouseModel
    private lateinit var binding: SavingPageBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(insertInformationModel::class.java)
        viewModel2 = ViewModelProviders.of(this).get(insertSavingCouseModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.saving_page, container, false)

        setValue()
        binding.button6.setOnClickListener {
            update()
        }

        binding.toMainPage3.setOnClickListener {
            view?.findNavController()?.navigate(SavingPageDirections.actionSavingPageToMainPage())
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.option_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }

    fun setValue(){
        viewModel2.allSavingCouse.observe(this, Observer { saving ->
            saving.map {
                binding.editText.setText(it.savingCouse.toString())
                binding.editText2.setText(it.investCouse.toString())
                binding.editText3.setText(it.useCouse.toString())
                binding.editText4.setText(it.countDonwDay.toString())
            }
        })
    }
    fun update(){
        var saveMoneyCal = 0
        var dayMoneyCalculated = 0
        var saveMoney = binding.editText.text.toString()
        var investMoney = binding.editText2.text.toString()
        var useMoney = binding.editText3.text.toString()
        viewModel2.allSavingCouse.observe(this, Observer { saving ->
            saving.map {
                saveMoneyCal = it.savingMoney
                dayMoneyCalculated = it.countDonwDay
            }
        })
        var targetMoney = 0
        viewModel.allUser.observe(this, Observer { saving ->
            saving.map {
                targetMoney = it.target
            }
        })

        Handler().postDelayed({
            var targetNow = targetMoney-saveMoneyCal
            var savingNow = saveMoney.toInt()+investMoney.toInt()
            var totalSaving = saveMoneyCal+savingNow
            dayMoneyCalculated = targetNow/savingNow
            Log.i("check",dayMoneyCalculated.toString()+ " "+totalSaving)
            if(dayMoneyCalculated >= 0 || saveMoneyCal < 50000){
            viewModel2.insert(
                user_saving(
                    0,
                    1,
                    saveMoney.toInt(),
                    investMoney.toInt(),
                    useMoney.toInt(),
                    dayMoneyCalculated,
                    totalSaving
                )
            )
                viewModel2.allSavingCouse.observe(this, Observer { saving ->
                    saving.map {
                        saveMoneyCal = it.savingMoney
                        if(saveMoneyCal >= 50000){
                            binding.editText4.setText("สำเร็จแล้วครับ")
                        }
                    }
                })

            }else{
                binding.editText4.setText("สำเร็จแล้วครับ")
            }

        }, 6000)
    }


}
