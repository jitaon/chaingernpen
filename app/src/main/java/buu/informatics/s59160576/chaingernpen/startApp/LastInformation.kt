package buu.informatics.s59160576.chaingernpen.startApp


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import buu.informatics.s59160576.chaingernpen.R
import buu.informatics.s59160576.chaingernpen.database.user_saving
import buu.informatics.s59160576.chaingernpen.database.userinformation
import buu.informatics.s59160576.chaingernpen.databinding.LastInformationPageBinding

/**
 * A simple [Fragment] subclass.
 */
class LastInformation : Fragment() {
    private lateinit var viewModel: insertInformationModel
    private lateinit var viewModel2: insertSavingCouseModel
    private lateinit var binding: LastInformationPageBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProviders.of(this).get(insertInformationModel::class.java)
        viewModel2 = ViewModelProviders.of(this).get(insertSavingCouseModel::class.java)
        binding = DataBindingUtil.inflate(inflater, R.layout.last_information_page, container, false)
        var model = arguments?.getString("allData")
        Log.i("checkAllData",model.toString())
        var str = model.toString()
        var delimiter1 = " "
        val parts = str?.split(delimiter1)

        save_couse_calculated()
        invest_money_calculated()
        count_day_calculated()
        use_couse()
        binding.button5.setOnClickListener { view : View ->
            initialDatabase(binding)
            view?.findNavController()?.navigate(LastInformationDirections.actionLastInformationToMainPage())
        }

        return binding.root
    }

    private fun save_couse_calculated():Int{
        var savingMoney = 0
        savingMoney = (debt_calculated()/100) * 15
        binding.savingMoney.setText(savingMoney.toString())
        return savingMoney
    }

    private fun invest_money_calculated():Int{
        var ans = (debt_calculated()/100) * 15
        binding.investMoney.setText(ans.toString())
        return ans
    }

    private fun count_day_calculated():Int{
        var ans = 0
        var model = arguments?.getString("allData")
        var str = model.toString()
        var delimiter1 = " "
        val parts = str?.split(delimiter1)
        ans = parts!![2].toInt()/(invest_money_calculated()+save_couse_calculated())
        binding.conutDownDay.setText(ans.toString()+" เดือน")
        return ans
    }

    private fun use_couse():Int{
        var ans = 0
        var model = arguments?.getString("allData")
        var str = model.toString()
        var delimiter1 = " "
        val parts = str?.split(delimiter1)
        ans = parts!![6].toInt() - (debt_calculated() + invest_money_calculated() + save_couse_calculated())
        binding.useMoney.setText(ans.toString())
        return ans
    }


    private fun  debt_calculated(): Int {
        var model = arguments?.getString("allData")
        var str = model.toString()
        var delimiter1 = " "
        val parts = str?.split(delimiter1)
        var ans = parts!![6].toInt() - (parts!![7].toInt()+parts!![8].toInt() + parts!![9].toInt()+parts!![10].toInt())
        return ans
    }

    private fun initialDatabase(binding: LastInformationPageBinding) {
        var count = 0
        var model = arguments?.getString("allData")
        var str = model.toString()
        var delimiter1 = " "
        val parts = str?.split(delimiter1)
        viewModel.allUser.observe(this, Observer {
            it.forEach {
                count++
            }
        })
        viewModel.insert(userinformation(
            count.toLong(),
            parts!![0],
            parts!![1].toInt(),
            parts!![2].toInt(),
            parts!![3],
            parts!![4],
            parts!![5].toInt(),
            parts!![6].toInt(),
            parts!![7].toInt(),
            parts!![8].toInt(),
            parts!![9].toInt(),
            parts!![10].toInt()
        ))

        viewModel2.insert(user_saving(
            count.toLong(),
            count.toLong(),
            save_couse_calculated(),
            invest_money_calculated(),
            use_couse(),
            count_day_calculated(),
            0
        ))

        Log.i("checkdata","insert success")
    }


}
