package buu.informatics.s59160576.chaingernpen.startApp

import androidx.lifecycle.LiveData
import buu.informatics.s59160576.chaingernpen.database.savingDatabaseDao
import buu.informatics.s59160576.chaingernpen.database.user_saving
import buu.informatics.s59160576.chaingernpen.database.userinformation
import buu.informatics.s59160576.chaingernpen.database.userinformationDatabaseDao

class InsertInformationRepository(private val userDao: userinformationDatabaseDao) {


    fun insert(user: userinformation) {
        userDao.insert(user)
    }

    val getAllUser: LiveData<List<userinformation>> = userDao.getAllUser()

}

class InsertSavingCouseRepository(private val saving: savingDatabaseDao) {


    fun insert(save: user_saving) {
        saving.insert(save)
    }

    fun update(save: user_saving) {
        saving.update(save)
    }

    val getAllSaving: LiveData<List<user_saving>> = saving.getAllSaving()

}
