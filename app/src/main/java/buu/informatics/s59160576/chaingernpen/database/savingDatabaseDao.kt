package buu.informatics.s59160576.chaingernpen.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface savingDatabaseDao{
    @Insert
    fun insert(saving: user_saving)

    @Update
    fun update(saving: user_saving)

    @Query("SELECT * from user_saving WHERE savingID = :key")
    fun get(key: Long): user_saving?

    @Query("DELETE FROM user_saving")
    fun clear()


    @Query("SELECT * FROM user_saving ORDER BY savingID DESC LIMIT 1")
    fun getAllSaving(): LiveData<List<user_saving>>
}