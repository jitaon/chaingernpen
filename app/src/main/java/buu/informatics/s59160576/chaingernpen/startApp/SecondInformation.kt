package buu.informatics.s59160576.chaingernpen.startApp


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.informatics.s59160576.chaingernpen.R
import buu.informatics.s59160576.chaingernpen.databinding.SecondInformationPageBinding
import buu.informatics.s59160576.chaingernpen.information_user1

/**
 * A simple [Fragment] subclass.
 */
class SecondInformation : Fragment() {
    private lateinit var binding: SecondInformationPageBinding

    private var insert_information_user: ArrayList<information_user1> = ArrayList<information_user1>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.second_information_page, container, false)
        var model = arguments?.getString("firstData")


        binding.button4.setOnClickListener { view: View ->
            checkData(model.toString())

        }

        binding.button.setOnClickListener { view: View ->
            view.findNavController().navigate(SecondInformationDirections.actionSecondInformationToFristInformation2())
        }

        return binding.root

    }

    private fun checkData(model:String) {
        if(binding.jobInput.text.isEmpty()){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else if(binding.jobAgeInput.text.isEmpty()){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else if(binding.startSalaryInput.text.isEmpty()){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else if(binding.salaryInput.text.isEmpty()){
            Toast.makeText(activity, "กรอกข้อมูลให้ครบถ้วน", Toast.LENGTH_SHORT).show()
        }else{
            view?.findNavController()?.navigate(SecondInformationDirections.actionSecondInformationToThridInformation(
                model+ " " + binding.jobInput.text.toString()+" "+binding.jobAgeInput.text.toString()+" "+binding.startSalaryInput.text.toString()+" "+binding.salaryInput.text.toString()
            ))
        }
    }
}
